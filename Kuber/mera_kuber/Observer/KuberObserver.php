<?php
namespace Kuber\mera_kuber\Observer;
use \Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
//use Magento\Framework\ObjectManager\ObjectManager;
//use \Magento\Framework\App\ObjectManager as Instance
define('ROOT_PATH','/var/www/html/meracash-magento/merakuber/magento/app/code/Kuber/Kuber_PHP_KIT/');
require_once ROOT_PATH.'utility/Holder.php';
require_once ROOT_PATH.'utility/UserBalance.php';
require_once ROOT_PATH.'utility/Callback.php';
require_once ROOT_PATH.'utility/KuberPaymentTransactionCallback.php';
require_once ROOT_PATH.'lib/config_kuber.php';
require_once ROOT_PATH.'utility/KuberPGService.php';
require_once ROOT_PATH.'utility/LiveService.php';
use kuber\utility\KuberPGService;
use kuber\utility\LiveService;
use kuber\utility\KuberPaymentTransactionCallback;
use kuber\utility\Holder;
use kuber\utility\UserBalance;
class KuberObserver implements ObserverInterface {
  protected $messageManager;
     protected $_responseFactory;
     protected $_url;
       protected $_logger;
       protected $observer;
      public function __construct(\Psr\Log\LoggerInterface $logger,\Magento\Framework\Message\ManagerInterface $messageManager,
      \Magento\Framework\App\ResponseFactory $responseFactory,
      \Magento\Framework\UrlInterface $url)
     {
         $this->messageManager = $messageManager;
         $this->_responseFactory = $responseFactory;
         $this->_url = $url;
         $this->_logger = $logger;
     }
  private function deductfromUser($total_amount,$mob){
    $service=new KuberPGService(new LiveService());
    $holder=new Holder();
    $holder->service=$service;
    $holder->total_amount=$total_amount;
    $holder->mob=$mob;
    $holder->messageManager=$this->messageManager;
    $service->getUserAccountBalance($mob,new class($holder) implements UserBalance{
      private $holder;
      public function __construct($holder)
                 {$this->holder = $holder; }
      public function balance($amount){
    //  echo "user balance=".$amount;
      error_log("user balance=".$amount,0);
      $this->holder->messageManager->addSuccess(__("Previous Kuber Balance: ".$amount));

    $kuber_need_to_deduct=0;
    if($this->holder->total_amount>=$amount){
$kuber_need_to_deduct=$amount;
}else $kuber_need_to_deduct=$this->holder->total_amount;
  error_log("kuber deducted=".$kuber_need_to_deduct,0);
  $this->holder->amount=$kuber_need_to_deduct;
  $this->holder->remaining=$this->holder->total_amount-$kuber_need_to_deduct;
$this->holder->service->deductFromUserAccount($this->holder->mob,$kuber_need_to_deduct,new class($this->holder) implements  KuberPaymentTransactionCallback {
  private $holder;
  public function __construct($holder)
             {$this->holder = $holder; }
                   public function success() {
                  // echo "success <br>";
                  error_log("success ",0);
                  $this->holder->messageManager->addSuccess(__($this->holder->amount." kuber sucessfully deducted from your kuber account."));
                  $this->holder->messageManager->addSuccess(__("You need to pay ".$this->holder->remaining. " in cash/other payemtn mode"));
                   }
                   public function insufficientFund() {
                    // echo "insufficientFund <br>";
                    error_log("insufficientFund",0);
                    $this->holder->messageManager->addNotice(__("insufficientFund in your kuber account."));
                   }
                   public function unauthorized() {
                  //  echo "unauthorized <br>";
                  error_log("unauthorized",0);
                  $this->holder->messageManager->addNotice(__("merchant is not unauthorized to access your kuber account."));
                   }
               });
      }
      public function error(){
      //  echo "error occured in getting balance. <br>";
      error_log("error occured in getting balance.",0);
      }
    });
         }
private function addKuberTouser($mob,$kuber){
  $holder=new Holder();
  $holder->mob=$mob;
  $holder->amount=$kuber;
  $holder->messageManager=$this->messageManager;
    $service=new KuberPGService(new LiveService());
    $service->addToUserAccount($mob,$kuber,new class($holder) implements  KuberPaymentTransactionCallback {
      private $holder;
      public function __construct($holder)
                 {$this->holder = $holder; }
                 public function success() {
                 //echo "success <br>";
                   error_log("success",0);
                     $this->holder->messageManager->addSuccess(__($this->holder->amount." kuber added to your kuber account."));
                 }
                 public function insufficientFund() {
                   //echo "insufficientFund <br>";
                     error_log("insufficientFund",0);
                     $this->holder->messageManager->addWarning(__("error occured while adding ".$this->holder->amount." kuber to your kuber account."));
                 }
                 public function unauthorized() {
                //  echo "unauthorized <br>";
                  error_log("unauthorized",0);
                  $this->holder->messageManager->addWarning(__("error occured while adding ".$this->holder->amount." kuber to your kuber account."));
                 }
             });
}

private function onOrderSuccess(){
 $order = $this->observer->getEvent()->getOrder();
 $customerId = $order->getCustomerId();
$items=$order->getAllItems();
  $kuber=0;
  $total_amount=0;
for($i=0;$i<count($items);$i++){
  $item=$items[$i];
  $product=$item->getProduct();
  if($product->getTypeId()=='simple') {
    $qty=$item->getQtyOrdered();
    $kuber=$kuber+$product->getPrice()*$qty*5/100;
    $total_amount+=$product->getPrice();
  }
}
$final_kuber=round($kuber);
$total_amount=round($total_amount);
$mob=$order->getBillingAddress()->getTelephone();
//$country_code=$order->getBillingAddress()->getCountry();
$country_code="+91";
$currency_code=$order->getOrderCurrencyCode();
$this->_logger->info("order",[
  "item_count"=>count($items),
  "total_kuber"=>$kuber,
  "rounded_kuber"=> $final_kuber,
  "mobile"=> $mob,
  "country_code"=>$country_code,
  "currency_code"=>$currency_code,
  "total_amount"=>$total_amount,
]);
$this->addKuberTouser($country_code.$mob,$final_kuber);
$this->deductfromUser($total_amount,$country_code.$mob);
     }

private function onCheckout(){
$cart=$this->observer->getCart();
$quote = $cart->getQuote();
$subTotal =$quote->getSubtotal();
$grandTotal =$quote->getGrandTotal();
$user_kuber_balance=0;
$kuber_need_to_deduct=0;
$grand_total_new=$grandTotal;
if($grand_total_new>=$user_kuber_balance){
  $kuber_need_to_deduct=$user_kuber_balance;
}
else $kuber_need_to_deduct=$grand_total_new;
//$billing_address=$quote->getBillingAddress();
$quote->setSubtotal($grand_total_new-$kuber_need_to_deduct);
$quote->setBaseSubtotal($grand_total_new-$kuber_need_to_deduct);
$quote->setGranTotal($grand_total_new-$kuber_need_to_deduct);
$quote->collectTotals()->save();
$mob=$quote->getBillingAddress()->getTelephone();
$country_code="+91";
//$quote->save($quote->setTotalsCollectedFlag(false)->collectTotals());
//$cart->save();
//$quote->save($quote->setTotalsCollectedFlag(false)->collectTotals());
$this->_logger->info("order",[
  "total"=>$subTotal." ".$grandTotal." ".$grand_total_new,
]);
$service=new KuberPGService(new LiveService());
/*$service->getUserAccountBalance($country_code.$mob,new class implements UserBalance{
  public function balance($amount){
//  echo "user balance=".$amount;
  error_log("user balance=".$amount,0);
  }
  public function error(){
  //  echo "error occured in getting balance. <br>";
  error_log("error occured in getting balance.",0);
  }
});*/
     }
private function applyKuberDiscount(){
$result = $observer->getEvent()->getResult();
$user_kuber_balance=100;
$discount=0;
if($user_kuber_balance>100){}
$result->setAmount($DiscountAmoun);
$result->setBaseAmount($DiscountAmoun);
}
    public function execute(\Magento\Framework\Event\Observer $observer) {
      //  $order = $observer->getEvent()->getOrder();
      //  $customerId = $order->getCustomerId();
      $this->observer=$observer;
      $event = $observer->getEvent();
        $this->_logger->info("order",[
          "event_name"=>$event->getName(),
        ]);
        switch($event->getName()){
          case "sales_order_save_after":
          $this->onOrderSuccess();
          break;
          case "checkout_cart_save_after":
          $this->onCheckout();
          break;
          case "checkout_cart_save_after":
          $this->onCheckout();
          break;
        }
        /*ini_set("log_errors", 1);
ini_set("error_log", "/tmp/php-error.log");
error_log( "Hello, errors!" );*/
        //return;
    /*  $service=new KuberPGService(new LiveService());

        $service->addToUserAccount("+919955154740",200,new class($a) implements  KuberPaymentTransactionCallback {
          public function __construct($a)
                     {}
                     public function success() {
                     //echo "success <br>";
                       error_log("success", 3,LOG_PATH);
                     }
                     public function insufficientFund() {
                       //echo "insufficientFund <br>";
                         error_log("insufficientFund", 3,LOG_PATH);
                     }
                     public function unauthorized() {
                    //  echo "unauthorized <br>";
                      error_log("unauthorized", 3,LOG_PATH);
                     }
                 });
                 $service->deductFromUserAccount("+919955154740",200,new class($a) implements  KuberPaymentTransactionCallback {
                   public function __construct($a)
                              {}
                              public function success() {
                              //echo "success <br>";
                                error_log("success", 3,LOG_PATH);
                              }
                              public function insufficientFund() {
                                //echo "insufficientFund <br>";
                                  error_log("insufficientFund", 3,LOG_PATH);
                              }
                              public function unauthorized() {
                               //echo "unauthorized <br>";
                                 error_log("unauthorized", 3,LOG_PATH);
                              }
                          });
            $service->getUserAccountBalance("+919955154740",new class($a) implements UserBalance{
              public function __construct($a)
                         {}
              public function balance($amount){
              //echo "user balance=".$amount;
              error_log("user balance=".$amount, 3,LOG_PATH);
              }
              public function error(){
                //echo "error occured in getting balance. <br>";
                error_log("error occured in getting balance.", 3,LOG_PATH);
              }
            });*/
      // die("hello");
        //print_r($order);
        //die;
      //  if($customerId)
            #do something with order an customer
    }
}
