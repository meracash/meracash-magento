<?php
namespace kuber\utility;
require_once ROOT_PATH.'utility/Callback.php';
require_once ROOT_PATH.'utility/ResponseManager.php';
require_once ROOT_PATH.'utility/Holder.php';
class KuberPGService {
private $stype;
private  $TAG="KuberPGService";
public function __construct($type){
$this->stype=$type;
}
public function addToUserAccount($mob,$amount,$callback){
$holder=new Holder();
$holder->callback=$callback;
$this->pay($mob, $amount,"INR", 0, new class($holder) implements Callback {
  private $holder;
  public function __construct($holder)
             {$this->holder = $holder; }
    public function callback($o, $response_code) {
      if($o==null){
        $this->holder->callback->insufficientFund();
        return;
      }
$this->holder->callback->success();
    }
});
}
public function deductFromUserAccount($mob,$amount,$callback){
  $holder=new Holder();
  $holder->callback=$callback;
    $this->pay($mob, $amount,"INR", 1, new class($holder) implements Callback {
      private $holder;
      public function __construct($holder)
                 {$this->holder = $holder; }
        public function callback($o, $response_code) {
          if($o==null){
            $this->holder->callback->insufficientFund();
            return;
          }
          $this->holder->callback->success();
        }
    });
}
public function getUserAccountBalance($mob,$balance){
  $holder=new Holder();
  $holder->callback=$balance;
  $holder->stype=$this->stype;
  $this->getUserAuth(AUTH_TOKEN,AUTH_SECRET, $mob, new class($holder) implements Callback {
    private $holder;
    public function __construct($holder)
               {$this->holder = $holder; }
      public function callback($o,$response_code) {
          if($o==null) {$this->holder->callback->error();}
          $authorization=$o;
          $url=$this->holder->stype->getUserBalanceURL();
          $header = array();
          $header[]="authorization: ".$authorization;
          $holder=new Holder();
          $holder->callback=$this->holder->callback;
          $manager=new ResponseManager(ResponseManager::$REQUEST_TYPE_GET,$url, $header, null,new class($holder) implements Callback {
            private $holder;
            public function __construct($holder)
                       {$this->holder = $holder; }
              public function callback($o, $response_code) {
                           if($o==null){
                             $this->holder->callback->callback(null,$res_code);
                           return;
                           }
                      $res=json_decode($o,true);
                      $amount=$res["availble_balance"]["balance"];
                       $this->holder->callback->balance($amount);
              }
          });
          $manager->get();
      }
  });

}
private  function getMercahntAuthToken($callback) {
        $url = $this->stype->getMerchantAuthtokenURL();
        $header = array();
        $header[]="authorization: ".AUTHORIZATION;
        $holder=new Holder();
        $holder->callback=$callback;
        $manager = new ResponseManager(ResponseManager::$REQUEST_TYPE_GET,$url,$header, null, new class($holder) implements Callback {
          private $holder;
          public function __construct($holder)
                     {$this->holder = $holder; }
            public function callback($response,$res_code){
                if ($response == null) {
                    $this->holder->callback->callback(null,$res_code);
                    return;
                }
               try { $ob=json_decode($response,true);
                    $auth_obj=$ob["auth"];
                    $auth_token=$auth_obj["auth_token"];
                    $auth_secret=$auth_obj["auth_secret"];
                    $map=array();
                    $map["auth_token"]=$auth_token;
                    $map["auth_secret"]=$auth_secret;
                    $this->holder->callback->callback($map,$res_code);
                }catch (Exception $e){
                    $this->holder->callback->callback(null,$res_code);
                }

            }
        });
        $manager->get();
    }
function getUserAuth($auth_token,$auth_secret,$mob,$callback) {
        $url = $this->stype->getUserAuthtokenURL();
        $base64 = base64_encode($mob);
        $url.="?mobile=".$base64;
        $header = array();
      //  $header[]="authorization: ".AUTHORIZATION;
        $header[]= "auth_token: ".$auth_token;
        $header[]="auth_secret: ".$auth_secret;
        $holder=new Holder();
        $holder->callback=$callback;
        $manager = new ResponseManager(ResponseManager::$REQUEST_TYPE_GET, $url, $header, null,new class($holder) implements Callback {
          private $holder;
          public function __construct($holder)
                     {$this->holder = $holder; }
            public function callback($response,$res_code) {
                if ($response == null) {
                    $this->holder->callback->callback(null,$res_code);
                    return;
                }
               try { $ob=json_decode($response,true);
                    $token_type=$ob["token_type"];
                    $access_token=$ob["access_token"];
                    $this->holder->callback->callback($token_type." ".$access_token,$res_code);
                }catch (Exception $e){
                    $this->holder->callback->callback(null,$res_code);
                }

            }
        });
        $manager->get();
    }


public function pay($mob,$amount,$currency_code,$transaction_type,$callback){
  $holder=new Holder();
  $holder->callback=$callback;
  $holder->mthis=$this;
  $holder->mob=$mob;
  $holder->amount=$amount;
  $holder->currency_code=$currency_code;
  $holder->transaction_type=$transaction_type;
  $holder->mob=$mob;
$holder->auth_token=AUTH_TOKEN;
$holder->auth_secret=AUTH_SECRET;
$this->getUserAuth($holder->auth_token,$holder->auth_secret,$holder->mob, new class($holder) implements Callback {
                  private $holder;
                  public function __construct($holder)
                             {$this->holder = $holder; }
                    public function callback($o,$response_code) {
                        if($o==null) {$this->holder->callback->callback(null,$response_code);}
                        $authorization=$o;
                        $this->holder->mthis->payUtil($this->holder->mob,$this->holder->amount,$this->holder->currency_code,$this->holder->auth_token, $this->holder->auth_secret,$authorization,$this->holder->transaction_type, new class($this->holder) implements Callback {
                          private $holder;
                          public function __construct($holder)
                                     {$this->holder = $holder; }
                            public function callback($o,$response_code) {
                                if($o==null) {$this->holder->callback->callback(null,$response_code);}

                                $this->holder->callback->callback("Success",$response_code);
                            }
                        });
                    }
                });

    }
 function payUtil($mob,$amount,$currency_code,$auth_token,$auth_secret,$authorization,$transaction_type,$callback){
    $url="";
    if($transaction_type==1)
    $url = $this->stype->getDeductFromUserURL();
    else $url=$this->stype->getaddAmountToUserURL();
        $header = array();
        $header[]="authorization: ".$authorization;
        $header[]="auth_token: ". $auth_token;
        $header[]="auth_secret: ".$auth_secret;
    $jbody = array();
        $jbody["amount"]=$amount;
        $jbody["currency_code"]=$currency_code;
        $jbody["payee_mobile"]=$mob;
        $holder=new Holder();
        $holder->callback=$callback;
        $manager = new ResponseManager(ResponseManager::$REQUEST_TYPE_POST, $url, $header, $jbody, new class($holder) implements Callback {
          private $holder;
          public function __construct($holder)
                     {$this->holder = $holder; }
            public function callback($response,$res_code) {
                if ($response == null) {
                    $this->holder->callback->callback(null,$res_code);
                    return;
                }

                $this->holder->callback->callback("Success",$res_code);

            }
        });
        $manager->get();
    }
}
?>
