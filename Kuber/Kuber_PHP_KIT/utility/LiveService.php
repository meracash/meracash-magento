<?php
namespace kuber\utility;
include('ServiceType.php');
use kuber\utility\ServiceType;
 class LiveService implements ServiceType {
private $BASE_URL="http://139.59.67.4/v1";
    public function getUserBalanceURL() {
        return $this->BASE_URL."/coupon/balance";
    }
    public function getMerchantAuthtokenURL() {
        return $this->BASE_URL."/merchant_auth/token";
    }
    public function getUserAuthtokenURL() {
        return $this->BASE_URL."/merchant_auth/user_auth";
    }
    public function getaddAmountToUserURL() {
      return $this->BASE_URL."/coupon/pay";
    }
    public function getDeductFromUserURL() {
        return $this->BASE_URL."/coupon/payment";
    }
}
?>
