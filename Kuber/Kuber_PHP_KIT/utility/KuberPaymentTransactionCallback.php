<?php
namespace kuber\utility;
interface KuberPaymentTransactionCallback {
function success();
function insufficientFund();
function unauthorized();
}
?>
