<?php
namespace kuber\utility;
interface ServiceType {
function getUserBalanceURL();
function getMerchantAuthtokenURL();
function getUserAuthtokenURL();
function getaddAmountToUserURL();
function getDeductFromUserURL();
}
?>
