<?php
namespace kuber\utility;
class ResponseManager {
    private  $TAG = "ResponseManager";
    private $URL;
    public  static $REQUEST_TYPE_GET = 0;
    public  static $REQUEST_TYPE_PUT = 1;
    public  static $REQUEST_TYPE_POST = 2;
    private $REQUEST_TYPE;
    private  $header;
    private $body;
    private $callback;
    public function __construct($REQUEST_TYPE, $URL, $header, $body,$callback) {
        $this->URL = $URL;
        $this->header = $header;
        $this->body = $body;
        $this->REQUEST_TYPE = $REQUEST_TYPE;
        $this->callback=$callback;
    }
public function util($url,$opearation,$body=null,$header=null){
    $curl = curl_init();
    $url=$this->URL;
    if($header==null)
    $header=array();
    $header[]='accept: application/json';
    curl_setopt_array($curl, array(
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 300,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => $opearation,
      CURLINFO_HEADER_OUT => true
    ));
    if($body!=null && !empty($body)){
      curl_setopt($curl, CURLOPT_POSTFIELDS,$body);
    }

    if($header!=null && !empty($header)){
      curl_setopt($curl, CURLOPT_HTTPHEADER,$header);
    }

    $response = curl_exec($curl);
    $err = curl_error($curl);
    $info=curl_getinfo($curl);
    // print_r($info["request_header"]);
    error_log($info["request_header"], 0);
  //  echo "<br> --------------------------------------------------- <br>";
  //  print_r($response);
  //  echo "<br>---------------------------------------------------  <br>";
    error_log($response, 0);
    curl_close($curl);

    return $response;
    	}
    public function get() {
      $get=0;
      $put=1;
      $post=2;
      switch($this->REQUEST_TYPE){
        case $get:
        $res=$this->util($this->URL,"GET",$this->body,$this->header);
        break;
        case $put:
        $res=$this->util($this->URL,"PUT",$this->body,$this->header);
        break;
        case $post:
        $res=$this->util($this->URL,"POST",$this->body,$this->header);
        break;
      }
      $this->callback->callback($res,200);
    }
  }
?>
