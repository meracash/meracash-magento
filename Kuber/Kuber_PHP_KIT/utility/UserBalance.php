<?php
namespace kuber\utility;
interface UserBalance {
    function balance($amount);
    function error();
}
?>
